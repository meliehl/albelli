# Orders.Api
This project is WebApi, using mongoDb as Database.
Basically I chose Mongo to not deal with Schemas.
I've tried separating the write logic (commands) from the read (query) logic.

Even in a simple CRUD project I could see some benefits, such as not having to return the same "Entity".
And for other improvments, I could use another database for read, or even other strategies, like read from secondary clusters.

Since we have only simple rules, I opted for integration tests. That came with costs, like running a mongo instance.

# Orders.WebApp
This project is an angular 6 project, hosted with dotnetcore

# Docker and Docker Compose
All projects (Api and Web App), has their own dockerfile.
And all projects could be run using docker-compose (with a mongo instance).

## Further Read
Checkout other projects on my [github] (https://github.com/meliehl)
