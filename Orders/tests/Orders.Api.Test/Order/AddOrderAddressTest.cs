using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Orders.Api.Order;
using Orders.Application.CommandSide.Commands;
using Xunit;

namespace Orders.Api.Test.Order
{
    public class AddOrderAddressTest: IClassFixture<TestServerFixture>
    {
        readonly HttpClient client;
        readonly string orderId;
        readonly AddOrderAddressRequest request;

        public AddOrderAddressTest(TestServerFixture factory)
        {
            client = factory.CreateClient();

            var response = OrderTestData.CreateOrder(client).Result;
            orderId = response.Id;

            request = new AddOrderAddressRequest()
            {
                City = "Test City",
                State = "Test State",
                Street = "Test Street",
                Neighborhood = "Test Neighborhood",
                Number = 100,
                PostCode = "1012AB",
            };
        }

        [Fact]
        public async Task WithValidRequestReturnsOK()
        {
            var result = await client.PutAsJsonAsync($"/api/order/{orderId}/address", request);
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
        }

        [Fact]
        public async Task WithInvalidPostCodeReturnsBadRequest()
        {
            request.PostCode = "1012aAb";
            var result = await client.PutAsJsonAsync($"/api/order/{orderId}/address", request);
            Assert.Equal(HttpStatusCode.BadRequest, result.StatusCode);
        }

        [Fact]
        public async Task WithInvalidNumberReturnsBadRequest()
        {
            request.Number = 0;
            var result = await client.PutAsJsonAsync($"/api/order/{orderId}/address", request);
            Assert.Equal(HttpStatusCode.BadRequest, result.StatusCode);
        }

        [Fact]
        public async Task WithNonExistisIdReturnsNotFoud()
        {
            var result = await client.PutAsJsonAsync($"/api/order/SAL12334409/address", request);
            Assert.Equal(HttpStatusCode.NotFound, result.StatusCode);
        }
    }
}
