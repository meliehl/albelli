using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Orders.Api.Order;
using Orders.Application.CommandSide.Commands;

namespace Orders.Api.Test.Order
{
    internal class OrderTestData
    {
        public static async Task<CreateOrderResult> CreateOrder(HttpClient client)
        {
            var createOrderRequest = new CreateOrderRequest()
            {
                Price = 10,
                CustomerName = "Jonh Doe"
            };

            var result = client.PostAsJsonAsync("/api/order", createOrderRequest).Result;
            var response = result.Content.ReadAsAsync<CreateOrderResult>().Result;
            return response;
        }
    }
}
