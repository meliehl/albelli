using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Orders.Api.Order;
using Orders.Application.CommandSide.Commands;
using Xunit;

namespace Orders.Api.Test.Order
{
    public class CreateOrderTest : IClassFixture<TestServerFixture>
    {
        private readonly HttpClient client;

        public CreateOrderTest(TestServerFixture factory) => client = factory.CreateClient();

        [Fact]
        public async Task WithValidRequestReturnsCreated()
        {
            var request = new CreateOrderRequest()
            {
                Price = 10,
                CustomerName = "Jonh Doe"
            };

            var result = await client.PostAsJsonAsync("/api/order", request);
            var response = await result.Content.ReadAsAsync<CreateOrderResult>();

            Assert.Equal(HttpStatusCode.Created, result.StatusCode);
            Assert.Contains("SAL", response.Id);
        }

        [Fact]
        public async Task WithInvalidPriceReturnsBadRequest()
        {
            var request = new CreateOrderRequest()
            {
                Price = -10,
                CustomerName = "Jonh Doe"
            };

            var result = await client.PostAsJsonAsync("/api/order", request);

            Assert.Equal(HttpStatusCode.BadRequest, result.StatusCode);
        }

        [Fact]
        public async Task WithInvalidCustomerNameReturnsBadRequest()
        {
            var request = new CreateOrderRequest()
            {
                Price = 0,
                CustomerName = string.Empty 
            };

            var result = await client.PostAsJsonAsync("/api/order", request);

            Assert.Equal(HttpStatusCode.BadRequest, result.StatusCode);
        }
    }
}
