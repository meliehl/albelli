using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Orders.Application.QuerySide.Queries;
using Xunit;

namespace Orders.Api.Test.Order
{
    public class GetOrderTest : IClassFixture<TestServerFixture>
    {
        readonly HttpClient client;
        readonly string orderId;

        public GetOrderTest(TestServerFixture factory)
        {
            client = factory.CreateClient();

            var response = OrderTestData.CreateOrder(client).Result;
            orderId = response.Id;
        }

        [Fact]
        public async Task WithExistisIdReturnsOK()
        {
            var result = await client.GetAsync($"/api/order/{orderId}");
            var response = await result.Content.ReadAsAsync<QueryOrderResult>();

            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
            Assert.Equal(orderId, response.Id);
        }

        [Fact]
        public async Task WithNonExistisIdReturnsNotFoud()
        {
            var result = await client.GetAsync($"/api/order/SAL12334409");
            Assert.Equal(HttpStatusCode.NotFound, result.StatusCode);
        }
    }
}
