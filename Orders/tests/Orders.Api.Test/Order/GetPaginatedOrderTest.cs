using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Orders.Api.Order;
using Orders.Application.QuerySide.Queries;
using Xunit;

namespace Orders.Api.Test.Order
{
    public class GetPaginatedOrderTest : IClassFixture<TestServerFixture>
    {
        readonly HttpClient client;
        readonly string orderId;

        readonly PaginatedOrderRequest request;
        public GetPaginatedOrderTest(TestServerFixture factory)
        {
            client = factory.CreateClient();

            for (var i = 0; i <= 11; i++)
            {
                var response = OrderTestData.CreateOrder(client).Result;
            }

            request = new PaginatedOrderRequest()
            {
                Page = 1,
                Qtdy = 10,
            };
        }

        [Fact]
        public async Task WithValidRequestReturnsOK()
        {
            var result = await client.GetAsync($"/api/order?page={request.Page}&qtdy={request.Qtdy}");
            var response = await result.Content.ReadAsAsync<QueryPaginatedOrderResult>();

            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
            Assert.Equal(10, response.Orders.Count());
        }

        [Fact]
        public async Task WithValidRequestPage2ReturnsOK()
        {
            request.Page = 2;
            var result = await client.GetAsync($"/api/order?page={request.Page}&qtdy={request.Qtdy}");
            var response = await result.Content.ReadAsAsync<QueryPaginatedOrderResult>();

            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
            Assert.NotEmpty(response.Orders);
        }

        [Fact]
        public async Task WithInvalidQueryReturnsBadRequest()
        {
            request.Qtdy = 200;
            var result = await client.GetAsync($"/api/order?page={request.Page}&qtdy={request.Qtdy}");
            var response = await result.Content.ReadAsAsync<QueryPaginatedOrderResult>();

            Assert.Equal(HttpStatusCode.BadRequest, result.StatusCode);
        }
    }
}
