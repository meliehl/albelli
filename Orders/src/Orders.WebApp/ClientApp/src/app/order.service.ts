import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http: HttpClient) { }


  public GetOrders(page: number): Observable<PaginatedOrder> {
   return this.http.get<PaginatedOrder>(`${environment.orderApi}/api/order?page=${page}&qtdy=10`);
  }

  public GetOrder(id: number): Observable<Order> {
   return this.http.get<Order>(`${environment.orderApi}/api/order/${id}`);
  }

  public postOrder(createOrderRequest: CreateOrderRequest): Observable<CreateOrderResult> {
    return this.http.post<CreateOrderResult>(`${environment.orderApi}/api/order`, createOrderRequest);
  }

  public putOrder(orderId: number,addOrderAddressRequest: AddOrderAddressRequest) {
    return this.http.put(`${environment.orderApi}/api/order/${orderId}/address`, addOrderAddressRequest);
  }
}
