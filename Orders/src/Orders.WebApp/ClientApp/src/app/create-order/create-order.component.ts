import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { OrderService } from '../order.service';

@Component({
  selector: 'app-create-order',
  templateUrl: './create-order.component.html',
  styleUrls: ['./create-order.component.css']
})
export class CreateOrderComponent implements OnInit {
  public addOrderForm: FormGroup;
  public unexpectedError: string[];

  constructor(private formBuilder: FormBuilder,
    private orderService: OrderService,
    private router: Router) {
  }

  ngOnInit() {
    this.addOrderForm = this.formBuilder.group({
      price: ["", [Validators.required, Validators.min(0)]],
      customerName: ["", [Validators.required, Validators.minLength(3)]],
    });
  }

  get price() {
    return this.addOrderForm.get("price");
  }

  get customerName() {
    return this.addOrderForm.get("customerName");
  }

  onSubmit() {
    console.log(this.addOrderForm.invalid);
    this.orderService.postOrder(this.addOrderForm.value)
      .subscribe(result => {
        console.log(result)
        this.router.navigate(['order/' + result.id]);
      }, error => {
        console.error(error);
        if (error.status == 400) {
          let serverError = error.error.errors;
          if (serverError.CustomerName) {
            this.customerName.markAsDirty();
            this.customerName.setErrors({
              "serverError": serverError.CustomerName
            })
          }

          if (serverError.Price) {
            this.price.markAsDirty();
            this.price.setErrors({
              "serverError": serverError.Price
            })
          }
        }
      });
  }

}
