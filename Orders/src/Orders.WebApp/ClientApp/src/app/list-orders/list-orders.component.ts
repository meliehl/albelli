import { Component } from '@angular/core';
import { OrderService } from '../order.service';

@Component({
  selector: 'app-list-orders',
  templateUrl: './list-orders.component.html',
  styleUrls: ['./list-orders.component.css']
})
export class ListOrdersComponent {
  public paginatedOrder: PaginatedOrder;
  public page: number;
  public enableLoadMore: boolean;

  constructor(private orderService : OrderService) {
    this.page = 1;
    this.setOrders(1);
  }

  private setOrders(page: number) {
    this.orderService.GetOrders(page).subscribe(result => {
      if (!this.paginatedOrder)
        this.paginatedOrder = result;
      else {
        this.paginatedOrder.page = result.page;
        this.paginatedOrder.qtdy = result.qtdy;
        this.paginatedOrder.total = result.total;
        this.paginatedOrder.orders = this.paginatedOrder.orders.concat(result.orders);
      }
      this.enableLoadMore = this.paginatedOrder.total > this.paginatedOrder.orders.length;
    }, error => console.error(error));
  }

  public loadMore() {
    this.setOrders(++this.page);
  }
    
  public refresh() {
    this.paginatedOrder.orders = [];
    this.page = 1;
    this.setOrders(this.page);
  }

  public formatAddress(order: Order): string {
    return order.city ? `${order.street}, ${order.number} - ${order.city} -  ${order.postCode}` : ''   ;
  }
}
