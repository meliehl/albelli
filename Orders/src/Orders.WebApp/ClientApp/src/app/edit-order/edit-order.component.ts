import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderService } from '../order.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-order',
  templateUrl: './edit-order.component.html',
  styleUrls: ['./edit-order.component.css']
})
export class EditOrderComponent implements OnInit {
  public order: Order;
  public id: number;
  public found?: boolean;
  public editOrderForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
    private orderService: OrderService,
    private router: Router,
    private route: ActivatedRoute) {

    this.editOrderForm = this.formBuilder.group({
      id: [],
      price: [],
      customerName: [],
      city: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
      state: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(30)]],
      street: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
      neighborhood: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(100)]],
      number: ['', [Validators.required, Validators.min(1)]],
      postCode: ['', [Validators.required, Validators.pattern("^[1-9][0-9]{3}\\s*(?:[a-zA-Z]{2})?$")]],
      additionalInformation: [],
    });

    this.route.params.subscribe(params => {
      this.id = params.id
    })
  }

  get city() {
    return this.editOrderForm.get("city");
  }

  get state() {
    return this.editOrderForm.get("state");
  }

  get street() {
    return this.editOrderForm.get("street");
  }

  get neighborhood() {
    return this.editOrderForm.get("neighborhood");
  }

  get number() {
    return this.editOrderForm.get("number");
  }

  get postCode() {
    return this.editOrderForm.get("postCode");
  }

  ngOnInit() {
    this.orderService.GetOrder(this.id).subscribe(result => {
      this.order = result;
      this.editOrderForm.setValue(result);
      this.found = true;
    }, error => {
      if (error.status = 404)
        this.found = false;
      console.error(error);
    });
  }

  onSubmit() {
    this.orderService.putOrder(this.id, this.editOrderForm.value).subscribe(result => {
      this.router.navigate(['orders']);
    }, error => {
      console.error(error);

      if (error.status = 400) {
        let serverError = error.error.errors;

        if (serverError.City) {
          this.city.markAsDirty();
          this.city.setErrors({
            "serverError": serverError.City
          })
        }

        if (serverError.State) {
          this.state.markAsDirty();
          this.state.setErrors({
            "serverError": serverError.State
          })
        }

        if (serverError.Street) {
          this.street.markAsDirty();
          this.street.setErrors({
            "serverError": serverError.Street
          })
        }

        if (serverError.Neighborhood) {
          this.neighborhood.markAsDirty();
          this.neighborhood.setErrors({
            "serverError": serverError.Neighborhood
          })
        }

        if (serverError.PostCode) {
          this.postCode.markAsDirty();
          this.postCode.setErrors({
            "serverError": serverError.PostCode
          })
        }
      }
    });
  }
}
