interface Order {
  id: string,
  price: number,
  customerName: string,
  city: string,
  state: string,
  street: string,
  neighborhood: string,
  number?: number,
  postCode: string,
  additionalInformation: string,
}
