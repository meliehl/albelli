interface PaginatedOrder {
  page: number,
  qtdy: number,
  total: number,
  orders: Order[],
}
