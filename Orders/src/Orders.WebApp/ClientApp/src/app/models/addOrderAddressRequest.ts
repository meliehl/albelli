interface AddOrderAddressRequest {
  city: string,
  state: string,
  street: string,
  neighborhood: string,
  number?: number,
  postCode: string,
  additionalInformation: string,
}
