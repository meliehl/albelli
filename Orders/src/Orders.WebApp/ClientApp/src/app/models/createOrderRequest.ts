interface CreateOrderRequest {
  price: number,
  customerName: string
}
