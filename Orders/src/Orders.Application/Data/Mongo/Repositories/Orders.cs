using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Driver;
using Orders.Application.CommandSide.Entities;
using Orders.Application.CommandSide.Repositories;

namespace Orders.Application.Data.Mongo
{
    public class Orders : IOrders
    {
        readonly IMongoCollection<Order> collection;
        public Orders(MongoProvider provider)
        {
            var db = provider.CreateDb;
            collection = db.GetCollection<Order>(nameof(Order));
        }

        public async Task<Order> Get(string id) => await collection.Find(f => f.Id == id).FirstOrDefaultAsync();

        public async Task Create(Order order) => await collection.InsertOneAsync(order);

        public async Task Update(Order order)
        {
            Expression<Func<Order, bool>> filter =
                f => f.Id == order.Id;

            await collection.ReplaceOneAsync(filter, order);
        }
    }
}
