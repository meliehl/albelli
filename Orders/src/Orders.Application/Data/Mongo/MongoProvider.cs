using MongoDB.Driver;

namespace Orders.Application.Data.Mongo
{
    public class MongoProvider
    {
        readonly MongoClient Client;
        public readonly MongoSettings settings;

        public MongoProvider(MongoSettings settings)
        {
            this.settings = settings;
            var mongoUrl = MongoUrl.Create(this.settings.Url);
            var clientSetings = new MongoClientSettings()
            {
                Server = mongoUrl.Server,
            };
            Client = new MongoClient(clientSetings);
        }

        public IMongoDatabase CreateDb => this.Client.GetDatabase(this.settings.Database);
    }
}
