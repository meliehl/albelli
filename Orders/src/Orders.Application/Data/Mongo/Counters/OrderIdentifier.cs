using MongoDB.Driver;
using Orders.Application.CommandSide.Entities;

namespace Orders.Application.Data.Mongo.Counters
{
    public class OrderIdentifier : IOrderIdentifier
    {
        readonly IMongoCollection<Counter> collection;
        public OrderIdentifier(MongoProvider provider)
        {
            var db = provider.CreateDb;
            collection = db.GetCollection<Counter>(nameof(Counter));
        }

        public string GetNew()
        {
            var filter = Builders<Counter>.Filter.Eq(a => a.Name, "order");
            var update = Builders<Counter>.Update.Inc(a => a.Value, 1);
            var counter = collection.FindOneAndUpdate(filter, update,
                new FindOneAndUpdateOptions<Counter, Counter>
                {
                    IsUpsert = true,
                    ReturnDocument = ReturnDocument.After,
                });

            return $"SAL{counter.Value}";
        }
    }
}
