using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Orders.Application.Data.Mongo.Counters
{
    class Counter
    {
        [BsonId]
        public ObjectId _Id { get; set; }
        public string Name { get; set; }
        public int Value { get; set; }
    }
}
