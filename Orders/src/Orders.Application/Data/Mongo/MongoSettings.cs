namespace Orders.Application.Data.Mongo
{
    public class MongoSettings
    {
        public string Url { get; set; }
        public string Database { get; set; }
    }
}
