namespace Orders.Application.CommandSide.Entities
{
    public class Address
    {
        public Address(string city, string state, string street, string neighborhood, uint number,string postCode, string additionalInformation)
        {
            City = city;
            State = state;
            Street = street;
            Neighborhood = neighborhood;
            Number = number;
            PostCode = postCode;
            AdditionalInformation = additionalInformation;
        }

        public string City { get; private set; }

        public string State { get; private set; }

        public string Street { get; private set; }

        public string Neighborhood { get; private set; }

        public uint Number { get; private set; }

        public string PostCode { get; private set; }

        public string AdditionalInformation { get; private set; }
    }
}
