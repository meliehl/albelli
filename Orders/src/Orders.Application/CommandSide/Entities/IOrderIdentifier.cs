using System;
using System.Collections.Generic;
using System.Text;

namespace Orders.Application.CommandSide.Entities
{
    public interface IOrderIdentifier
    {
        string GetNew();
    }
}
