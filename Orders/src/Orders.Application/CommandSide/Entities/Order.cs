namespace Orders.Application.CommandSide.Entities
{
    public class Order
    {
        internal Order()
        {

        }

        public static Order Create(IOrderIdentifier identifier, decimal price, string customerName)
        {
            var order = new Order
            {
                Id = identifier.GetNew(),
                Price = price,
                CustomerName = customerName
            };
            return order;
        }

        public void AddAddress(Address address) => Address = address;

        public string Id { get; private set; }

        public decimal Price { get; private set; }

        public string CustomerName { get; private set; }

        public Address Address { get; private set; }
    }
}