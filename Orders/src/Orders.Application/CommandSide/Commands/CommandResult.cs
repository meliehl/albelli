using System.Collections.Generic;
using System.Linq;

namespace Orders.Application.CommandSide.Commands
{
    public class CommandResult
    {

    }
    public class CommandResult<T> where T : CommandResult
    {
        public IDictionary<string, string[]> FailureReason { get; } = new Dictionary<string, string[]>();

        public T SuccessResult { get; }

        private CommandResult(T result) => SuccessResult = result;

        private CommandResult(IDictionary<string, string[]> failureReason) => FailureReason = failureReason;

        public bool IsSuccess => !FailureReason.Any();

        public static CommandResult<T> Success(T result) => new CommandResult<T>(result);

        public static CommandResult<T> NotFound() => new CommandResult<T>(default(T));


        public static CommandResult<T> Fail(IDictionary<string, string[]> reason) => new CommandResult<T>(reason);

        public static implicit operator bool(CommandResult<T> result) => result.IsSuccess;

    }
}
