using System.Threading.Tasks;
using Orders.Application.CommandSide.Entities;
using Orders.Application.CommandSide.Repositories;

namespace Orders.Application.CommandSide.Commands
{
    public class AddOrderAddressCommand : Command
    {
        public AddOrderAddressCommand(string orderId, string city, string state, string street, string neighborhood, uint number,
            string postCode, string additionalInformation)
        {
            OrderId = orderId;
            City = city;
            State = state;
            Street = street;
            Neighborhood = neighborhood;
            Number = number;
            PostCode = postCode;
            AdditionalInformation = additionalInformation;
        }

        public string OrderId { get; }

        public string City { get; }

        public string State { get; }

        public string Street { get; }

        public string Neighborhood { get; }

        public uint Number { get; }

        public string PostCode { get; }

        public string AdditionalInformation { get; }
    }

    public class AddOrderAddresResult : CommandResult
    {
        public AddOrderAddresResult(string id) => Id = id;

        public string Id { get; }
    }

    public class AddOrderAddresHandler : ICommandHandler<AddOrderAddressCommand, AddOrderAddresResult>
    {
        readonly IOrders orders;

        public AddOrderAddresHandler(IOrders orders) => this.orders = orders;

        public async Task<CommandResult<AddOrderAddresResult>> HandleAsync(AddOrderAddressCommand command)
        {
            var order = await orders.Get(command.OrderId);
            if (order == null)
                return CommandResult<AddOrderAddresResult>.NotFound();

            var address = new Address(command.City, command.State, command.Street, command.Neighborhood, command.Number, command.PostCode, command.AdditionalInformation);
            order.AddAddress(address);

            await orders.Update(order);

            var result = new AddOrderAddresResult(order.Id);

            return CommandResult<AddOrderAddresResult>.Success(result);
        }
    }
}
