using System.Threading.Tasks;
using Orders.Application.CommandSide.Entities;
using Orders.Application.CommandSide.Repositories;

namespace Orders.Application.CommandSide.Commands
{
    public class CreateOrderCommand : Command
    {
        public CreateOrderCommand(decimal price, string customerName)
        {
            Price = price;
            CustomerName = customerName;
        }

        public decimal Price { get; }

        public string CustomerName { get; }
    }

    public class CreateOrderResult : CommandResult
    {
        public CreateOrderResult(string id) => Id = id;

        public string Id { get; }
    }

    public class CreateOrderHandler : ICommandHandler<CreateOrderCommand, CreateOrderResult>
    {
        readonly IOrders orders;
        readonly IOrderIdentifier orderIdentifier;

        public CreateOrderHandler(IOrders orders, IOrderIdentifier orderIdentifier)
        {
            this.orders = orders;
            this.orderIdentifier = orderIdentifier;
        }

        public async Task<CommandResult<CreateOrderResult>> HandleAsync(CreateOrderCommand command)
        {
            var order = Order.Create(orderIdentifier,command.Price, command.CustomerName);

            await orders.Create(order);

            var result = new CreateOrderResult(order.Id);

            return CommandResult<CreateOrderResult>.Success(result);
        }
    }
}
