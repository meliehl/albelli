using System.Threading.Tasks;

namespace Orders.Application.CommandSide.Commands
{
    public interface ICommandHandler<TCommand, TResult> 
        where TCommand : Command
        where TResult : CommandResult
    {
        Task<CommandResult<TResult>> HandleAsync(TCommand command);
    }
}
