using System.Threading.Tasks;
using Orders.Application.CommandSide.Entities;

namespace Orders.Application.CommandSide.Repositories
{
    public interface IOrders
    {
        Task<Order> Get(string id);
        Task Create(Order order);
        Task Update(Order order);
    }
}
