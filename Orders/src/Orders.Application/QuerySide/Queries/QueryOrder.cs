using System.Threading.Tasks;
using MongoDB.Driver;
using Orders.Application.CommandSide.Entities;
using Orders.Application.Data.Mongo;

namespace Orders.Application.QuerySide.Queries
{
    public class QueryOrder : Query
    {
        public QueryOrder(string id)
        {
            Id = id;
        }

        public string Id { get; }
    }

    public class QueryOrderHandler : IQueryHandler<QueryOrder, QueryOrderResult>
    {
        readonly IMongoCollection<Order> orders;
        public QueryOrderHandler(MongoProvider provider)
        {
            var db = provider.CreateDb;
            orders = db.GetCollection<Order>(nameof(Order));
        }

        public async Task<QueryOrderResult> HandleAsync(QueryOrder query)
        {
            return await orders.Find(f => f.Id == query.Id).Project(p => new QueryOrderResult()
            {
                Id = p.Id,
                Price = p.Price,
                CustomerName = p.CustomerName,
                City = p.Address != null ? p.Address.City : string.Empty,
                State = p.Address != null ? p.Address.State : string.Empty,
                Street = p.Address != null ? p.Address.Street : string.Empty,
                Neighborhood = p.Address != null ? p.Address.Neighborhood : string.Empty,
                Number = p.Address != null ? (uint?)p.Address.Number : null,
                PostCode = p.Address != null ? p.Address.PostCode : string.Empty,
                AdditionalInformation = p.Address != null ? p.Address.AdditionalInformation : string.Empty,
            }).FirstOrDefaultAsync();
        }
    }
}
