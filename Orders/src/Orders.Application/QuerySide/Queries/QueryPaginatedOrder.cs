using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using Orders.Application.CommandSide.Entities;
using Orders.Application.Data.Mongo;

namespace Orders.Application.QuerySide.Queries
{
    public class QueryPaginatedOrder : Query
    {
        public QueryPaginatedOrder(uint page, uint qtdy)
        {
            Page = page;
            Qtdy = qtdy;
        }

        public uint Page { get; }
        public uint Qtdy { get; }
    }

    public class QueryPaginatedOrderResult : QueryResult
    {
        public uint Page { get; set; }
        public int Qtdy { get; set; }
        public long Total { get; set; }

        public IEnumerable<QueryOrderResult> Orders { get; set; }
    }

    public class QueryPaginatedOrderHandler : IQueryHandler<QueryPaginatedOrder, QueryPaginatedOrderResult>
    {
        readonly IMongoCollection<Order> orders;
        public QueryPaginatedOrderHandler(MongoProvider provider)
        {
            var db = provider.CreateDb;
            orders = db.GetCollection<Order>(nameof(Order));
        }
        public async Task<QueryPaginatedOrderResult> HandleAsync(QueryPaginatedOrder query)
        {
            var skip = (query.Page - 1) * query.Qtdy;

            var queryable = orders.Find(f => true);
            var total = queryable.CountDocumentsAsync();
            var result = queryable.Project(p => new QueryOrderResult()
            {
                Id = p.Id,
                Price = p.Price,
                CustomerName = p.CustomerName,
                City = p.Address != null ? p.Address.City : string.Empty,
                State = p.Address != null ? p.Address.State : string.Empty,
                Street = p.Address != null ? p.Address.Street : string.Empty,
                Neighborhood = p.Address != null ? p.Address.Neighborhood : string.Empty,
                Number = p.Address != null ? (uint?)p.Address.Number : null,
                PostCode = p.Address != null ? p.Address.PostCode : string.Empty,
                AdditionalInformation = p.Address != null ? p.Address.AdditionalInformation : string.Empty,
            })
            .Skip((int)skip)
            .Limit((int)query.Qtdy)
            .ToListAsync();

            await Task.WhenAll(total, result);

            return new QueryPaginatedOrderResult()
            {
                Page = query.Page,
                Qtdy = result.Result.Count,
                Total = total.Result,
                Orders = result.Result
            };
        }
    }
}
