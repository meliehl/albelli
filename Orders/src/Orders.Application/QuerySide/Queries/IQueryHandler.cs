using System.Threading.Tasks;

namespace Orders.Application.QuerySide.Queries
{
    public interface IQueryHandler<TQuery, TResult>
        where TQuery : Query
        where TResult : QueryResult
    {
        Task<TResult> HandleAsync(TQuery query);
    }

}
