namespace Orders.Application.QuerySide.Queries
{
    public class QueryOrderResult : QueryResult
    {
        public string Id { get; set; }

        public decimal Price { get; set; }

        public string CustomerName { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Street { get; set; }

        public string Neighborhood { get; set; }

        public uint? Number { get; set; }

        public string PostCode { get; set; }

        public string AdditionalInformation { get; set; }
    }
}
