using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Orders.Application.Data.Mongo;

namespace Orders.Api.StartupBlocks
{
    public static class ConfigureSettings
    {
        public static IServiceCollection AddSettings(this IServiceCollection services
            , IConfiguration configuration)
        {
            var mongoSettings = configuration.GetSection("Mongo").Get<MongoSettings>();
            services.AddSingleton(mongoSettings);

            return services;
        }

    }
}
