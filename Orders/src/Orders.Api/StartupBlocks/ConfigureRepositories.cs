using Microsoft.Extensions.DependencyInjection;
using Orders.Application.CommandSide.Repositories;

namespace Orders.Api.StartupBlocks
{
    public static class ConfigureRepositories
    {
        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services.AddScoped<IOrders, Application.Data.Mongo.Orders>();
            return services;
        }
    
    }
}
