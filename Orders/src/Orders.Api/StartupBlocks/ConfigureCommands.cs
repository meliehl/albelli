using Microsoft.Extensions.DependencyInjection;
using Orders.Application.CommandSide.Commands;

namespace Orders.Api.StartupBlocks
{
    public static class ConfigureCommands
    {
        public static IServiceCollection AddCommands(this IServiceCollection services)
        {
            services.AddScoped<ICommandHandler<CreateOrderCommand, CreateOrderResult>, CreateOrderHandler>();
            services.AddScoped<ICommandHandler<AddOrderAddressCommand, AddOrderAddresResult>, AddOrderAddresHandler>();

            return services;
        }

    }
}
