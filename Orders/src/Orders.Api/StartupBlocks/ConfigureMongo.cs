using Microsoft.Extensions.DependencyInjection;
using Orders.Application.CommandSide.Entities;
using Orders.Application.Data.Mongo;
using Orders.Application.Data.Mongo.Counters;

namespace Orders.Api.StartupBlocks
{
    public static class ConfigureMongo
    {
        public static IServiceCollection AddMongo(this IServiceCollection services)
        {
            services.AddScoped<MongoProvider>();
            services.AddScoped<IOrderIdentifier, OrderIdentifier>();
            return services;
        }

    }
}
