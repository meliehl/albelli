using Microsoft.Extensions.DependencyInjection;
using Orders.Application.QuerySide.Queries;

namespace Orders.Api.StartupBlocks
{
    public static class ConfigureQueries
    {
        public static IServiceCollection AddQueries(this IServiceCollection services)
        {
            services.AddScoped<IQueryHandler<QueryOrder, QueryOrderResult>, QueryOrderHandler>();
            services.AddScoped<IQueryHandler<QueryPaginatedOrder, QueryPaginatedOrderResult>, QueryPaginatedOrderHandler>();
            return services;
        }

    }
}
