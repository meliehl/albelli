using System.ComponentModel.DataAnnotations;

namespace Orders.Api.Order
{
    public class CreateOrderRequest
    {
        [Range(0,99999999)]
        public decimal Price { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(100)]
        public string CustomerName { get; set; }
    }
}
