using System.ComponentModel.DataAnnotations;

namespace Orders.Api.Order
{
    public class AddOrderAddressRequest
    {
        [Required]
        [MinLength(3)]
        [MaxLength(100)]
        public string City { get; set; }

        [Required]
        [MinLength(2)]
        [MaxLength(30)]
        public string State { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(100)]
        public string Street { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(100)]
        public string Neighborhood { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public uint Number { get; set; }

        [Required]
        [RegularExpression("^[1-9][0-9]{3}\\s*(?:[a-zA-Z]{2})?$")]
        public string PostCode { get; set; }

        public string AdditionalInformation { get; set; }
    }
}
