using System.ComponentModel.DataAnnotations;

namespace Orders.Api.Order
{
    public class PaginatedOrderRequest
    {
        [Required]
        public uint Page { get; set; }

        [Required]
        [Range(1, 50)]
        public uint Qtdy { get; set; }
    }
}
