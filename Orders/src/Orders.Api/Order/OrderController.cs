using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Orders.Application.CommandSide.Commands;
using Orders.Application.QuerySide.Queries;

namespace Orders.Api.Order
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        readonly ICommandHandler<CreateOrderCommand, CreateOrderResult> createCommand;
        readonly ICommandHandler<AddOrderAddressCommand, AddOrderAddresResult> addOrderAddressCommand;

        readonly IQueryHandler<QueryOrder, QueryOrderResult> queryOrder;
        readonly IQueryHandler<QueryPaginatedOrder, QueryPaginatedOrderResult> queryPaginatedOrder;


        public OrderController(ICommandHandler<CreateOrderCommand, CreateOrderResult> createCommand,
            ICommandHandler<AddOrderAddressCommand, AddOrderAddresResult> addOrderAddressCommand,
            IQueryHandler<QueryOrder, QueryOrderResult> queryOrder,
            IQueryHandler<QueryPaginatedOrder, QueryPaginatedOrderResult> queryPaginatedOrder)
        {
            this.createCommand = createCommand;
            this.addOrderAddressCommand = addOrderAddressCommand;
            this.queryOrder = queryOrder;
            this.queryPaginatedOrder = queryPaginatedOrder;
        }

        [HttpGet("{id}")]
        [ProducesResponseType(404)]
        [ProducesResponseType(200)]
        public async Task<ActionResult<QueryOrderResult>> GetById(string id)
        {
            var result = await queryOrder.HandleAsync(new QueryOrder(id));
            if (result == null)
                return NotFound();

            return Ok(result);
        }

        [HttpGet]
        [ProducesResponseType(400)]
        [ProducesResponseType(200)]
        public async Task<ActionResult<QueryPaginatedOrderResult>> Get([FromQuery]PaginatedOrderRequest request)
        {
            var result = await queryPaginatedOrder.HandleAsync(new QueryPaginatedOrder(request.Page, request.Qtdy));

            return Ok(result);
        }

        [HttpPost]
        [ProducesResponseType(400)]
        [ProducesResponseType(201)]
        public async Task<ActionResult<CreateOrderResult>> Post([FromBody]CreateOrderRequest request)
        {
            var command = new CreateOrderCommand(request.Price, request.CustomerName);
            var result = await createCommand.HandleAsync(command);
            return Created("api/v1/Order", result.SuccessResult);
        }

        [HttpPut("{id}/Address")]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(200)]
        public async Task<ActionResult<AddOrderAddresResult>> PutAddress(string id, [FromBody]AddOrderAddressRequest request)
        {
            var command = new AddOrderAddressCommand(id, request.City, request.State, request.Street, request.Neighborhood, request.Number,
                request.PostCode, request.AdditionalInformation);
            var result = await addOrderAddressCommand.HandleAsync(command);

            if (result.SuccessResult == default(AddOrderAddresResult))
                return NotFound(id);

            return Ok();
        }

    }
}